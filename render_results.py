#!/usr/bin/env python3

import os.path as op
from os import walk, makedirs
import json
from subprocess import check_output

INPUT_DIR = op.abspath(op.join(__file__, '..', 'results'))
OUTPUT_DIR = op.abspath(op.join(__file__, '..', 'doc', 'comparison'))

for root, folders, files_name in walk(INPUT_DIR):
    for file_name in files_name:
        input_path = op.join(root, file_name)
        output_path = op.join(OUTPUT_DIR, input_path.replace(INPUT_DIR, '', 1)[1:-5] + '.md')

        if not op.isdir(op.dirname(output_path)):
            makedirs(op.dirname(output_path))

        with open(input_path) as input_file, open(output_path, 'w') as output_file:
            input = json.loads(input_file.read())

            output_file.write('# %s\n\n' % input['name'])

            parsers = input['tests'][0]['results'].keys()
            output_file.write('| Feature | %s |\n| %s\n' % (
                ' | '.join([r.title() for r in parsers]),
                '------ |' * (len(parsers) + 1)
            ))

            for test in input['tests']:
                output_file.write('| %s ' % test['label'])
                for parser, result in test['results'].items():
                    cell = '✅' if result['stdout'] in test['expected'] else '❌'
                    # tooltip = (result['stdout'] + result['stderr'])
                    # output_file.write('| [%s]{%s} ' % (cell, tooltip))
                    output_file.write('| %s ' % cell)
                output_file.write('|\n')

# for test_suite_results_dir in os.listdir(RESULTS_DIR):
#     test_suite_results_dir=op.abspath(op.join(RESULTS_DIR, test_suite_results_dir))
#
#     parsers = sorted([op.basename(path) for path in os.listdir(test_suite_results_dir)])
#
#     print('\n## %s\n\n| Feature | %s |\n| ------- | %s' % (
#         op.basename(test_suite_results_dir).title(),
#         ' | '.join([r.title() for r in parsers]),
#         '------ |' * len(parsers)
#     ))
#
#     results = {}
#     for results_path in os.listdir(test_suite_results_dir):
#         results_path = op.abspath(op.join(test_suite_results_dir, results_path))
#
#         with open(results_path) as results_file:
#             for line in results_file.readlines():
#                 test_key, test_output = (line.strip().split(':', 1) + [''])[:2]
#                 if test_key not in results:
#                     results[test_key] = {}
#                 results[test_key][op.basename(results_path)] = test_output
#
#     for test_key, test_result in results.items():
#         print('| %s ' % test_key, end='')
#         for parser in parsers:
#             print('| `%s` ' % test_result[parser] if test_result[parser] else '| - ', end='')
#         print('|')
