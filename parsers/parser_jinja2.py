#!/usr/bin/env python3

from sys import argv, exit
import json
from jinja2 import Template

if len(argv) != 3:
    print("Usage: %s '<template>' '<json data>'" % argv[0])
    exit(1)

template = argv[1]
data = json.loads(argv[2])
print(Template(template).render(data))
