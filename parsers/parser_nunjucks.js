#!/usr/bin/env node

const nunjucks = require('nunjucks')

if (process.argv.length != 4) {
    console.log("Usage: %s '<template>' '<json data>'" % process.argv[1])
    process.exit(1);
}

nunjucks.configure({ autoescape: true })
template = process.argv[2]
data = JSON.parse(process.argv[3])
console.log(nunjucks.renderString(template, data))
