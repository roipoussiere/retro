#!/usr/bin/env php
<?php

require_once 'vendor/autoload.php';

if (count($argv) != 3) {
    echo "Usage: $argv[0] '<template>' '<json data>'\n";
    exit(1);
}

$template = $argv[1];
$data = json_decode($argv[2], true);
$loader = new \Twig\Loader\ArrayLoader([ "test" => $template ]);
$twig = new \Twig\Environment($loader);
echo $twig->render("test", $data) . "\n";
