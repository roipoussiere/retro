#!/usr/bin/env node

const Twig = require('twig')

if (process.argv.length != 4) {
    console.log("Usage: %s '<template>' '<json data>'" % process.argv[1])
    process.exit(1);
}

template = process.argv[2]
data = JSON.parse(process.argv[3])
console.log(Twig.twig({ data: template }).render(data))
