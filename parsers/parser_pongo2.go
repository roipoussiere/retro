//usr/bin/env go run "$0" "$@"; exit "$?"
package main

import (
    "os"
    "fmt"
    "encoding/json"
    "github.com/flosch/pongo2/v4"
)

func main() {
    if len(os.Args) != 3 {
        fmt.Println("Usage: test_pongo2.go '<template>' '<json data>'")
        os.Exit(1)
    }

    template, err := pongo2.FromString(os.Args[1]); if err != nil {
        panic(err)
    }

    var data map[string]interface{}
    json.Unmarshal([]byte(os.Args[2]), &data)

    output, err := template.Execute(data); if err != nil {
        panic(err)
    }
    fmt.Println(output)
}
