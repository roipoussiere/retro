#!/usr/bin/env python3

from os import walk, makedirs
import os.path as op
from sys import argv, exit
import json
from subprocess import Popen, PIPE

import yaml

PARSERS_DIR = op.abspath(op.join(__file__, '..', 'parsers'))
TEST_SUITES_DIR = op.abspath(op.join(__file__, '..', 'test_suites'))
RESULTS_DIR = op.abspath(op.join(__file__, '..', 'results'))

PARSERS_CMD = {
    'jinja2': ['python3', op.join(PARSERS_DIR, 'parser_jinja2.py')],
    'nunjucks': ['node', op.join(PARSERS_DIR, 'parser_nunjucks.js')],
    'pongo2': ['bash', '-c', 'cd %s && go run parser_pongo2.go' % PARSERS_DIR],
    'twig': ['php', op.join(PARSERS_DIR, 'parser_twig.php')],
    'twigjs': ['node', op.join(PARSERS_DIR, 'parser_twigjs.js')]
}

parsers = PARSERS_CMD.keys() if len(argv) == 1 else argv[1:]

for parser in parsers:
    if parser not in PARSERS_CMD.keys():
        print('Reader must be one of [%s].' % ', '.join(PARSERS_CMD.keys()))
        exit(1)

test_suites_path = []
for root, folders, files_name in walk(TEST_SUITES_DIR):
    for file_name in files_name:
        test_suites_path.append(op.join(root, file_name))

test_suites = {}
for test_suite_path in test_suites_path:
    test_suite_key = test_suite_path.replace(TEST_SUITES_DIR, '', 1)[1:-4]
    with open(test_suite_path) as stream:
        test_suite = yaml.safe_load(stream)

        for i_test, test in enumerate(test_suite['tests']):
            if not 'data' in test:
                test['data'] = '{}'
            if isinstance(test['expected'], str):
                test['expected'] = [test['expected']]

            for parser in parsers:
                if parser == 'pongo2':
                    # Python subprocess don't understand go shebang
                    cmd = "%s '%s' '%s'" % (PARSERS_CMD[parser][-1],
                                            test['template'], test['data'])
                    cmd = PARSERS_CMD[parser][:-1] + [cmd]
                else:
                    cmd = PARSERS_CMD[parser] + [test['template'], test['data']]
                process = Popen(cmd, stdout=PIPE, stderr=PIPE)

                if 'results' not in test:
                    test['results'] = {}
                test['results'][parser] = {'process': process}

            test_suite['tests'][i_test] = test
        test_suites[test_suite_key] = test_suite

for test_suite_key, test_suite in test_suites.items():
    result_path = op.join(RESULTS_DIR, test_suite_key) + '.json'
    if not op.isdir(op.dirname(result_path)):
        makedirs(op.dirname(result_path))

    for i_test, test in enumerate(test_suite['tests']):
        for engine, result in test['results'].items():
            result['stdout'] = result['process'].stdout.read().decode().strip()
            result['stderr'] = result['process'].stderr.read().decode().strip()
            result['process'].wait()
            result.pop('process')
            test_suite['tests'][i_test][engine] = result

    with open(result_path, 'w') as result_file:
        json.dump(test_suite, result_file, indent=2)
