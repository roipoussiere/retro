# Retro

A common subset of several template engines syntax:

- [Twig](https://twig.symfony.com) (PHP);
- [Jinja2](https://jinja2docs.readthedocs.io) (Python);
- [Pongo2](https://github.com/flosch/pongo2) (Golang)
- [Nunjuck](https://mozilla.github.io/nunjucks) (Javascript);
- [TwigJS](https://github.com/twigjs/twig.js) (Javascript);

## Running the test suite locally

### Using Docker

```
docker run -v $(pwd)/test_suites:/opt/test_suites -v $(pwd)/results:/opt/results roipoussiere/retro
```

First clone the repository:

```
git clone https://framagit.org/roipoussiere/retro
```

Next steps are language-specific and described in next sub-sections.

Once done, run the corresponding file (each of them are executable):

- Jinja2: `./test_jinja2.py`;
- Twig: `./test_twig.php`;
- Nunjucks: `./test_nunjucks.js`;
- Pongo2: `./test_pongo2.go`

### Jinja2 test suite

Requirements: [Python 3](https://www.python.org/)

```
python -m venv .venv
source .venv/bin/activate
pip install -r ./requirements.txt
```

### Twig test suite

Requirements: [PHP](https://www.php.net/) and [Composer](https://getcomposer.org/)

```
composer install
```

### Pongo2 test suite

Requirements: [Go](https://golang.org/)

```
go get
```

### Nunjucks test suite

Requirements: [NodeJS](https://nodejs.org/en/)

```
npm install
```

## Related projects

[Jinja compat test](https://jbmoelker.github.io/jinja-compat-tests/)

## Roadmap

- add [Pebble](https://pebbletemplates.io/) (Java template engine);
- more tests;
- generate a nice front-end to visualize results
